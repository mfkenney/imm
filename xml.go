package imm

import (
	"bytes"
	"encoding/xml"
	"errors"
	"fmt"
	"unicode"
)

type ErrImm struct {
	Type string `xml:"type,attr"`
	Msg  string `xml:"msg,attr"`
}

func (e ErrImm) Is(target error) bool {
	if t, ok := target.(ErrImm); ok && t.Type == e.Type {
		return true
	}
	return false
}

func (e ErrImm) Error() string {
	return fmt.Sprintf("%s: %s", e.Type, e.Msg)
}

type errImm struct {
	XMLName xml.Name `xml:"Error"`
	ErrImm
}

func (e *errImm) Raised() bool {
	return e.XMLName.Local == "Error"
}

// Alternate form of the error tag
type errImmAlt struct {
	XMLName xml.Name `xml:"ERROR"`
	ErrImm
}

func (e *errImmAlt) Raised() bool {
	return e.XMLName.Local == "ERROR"
}

type ErrTimeout struct {
	XMLName xml.Name `xml:"TIMEOUT"`
	Msg     string   `xml:"msg,attr"`
}

func (e *ErrTimeout) Raised() bool {
	return e.XMLName.Local == "TIMEOUT"
}

func (e *ErrTimeout) Error() string {
	return e.Msg
}

// Reply from the remote host
type RemoteReply struct {
	Raw  []byte `xml:",innerxml"`
	Text string `xml:",chardata"`
}

func (r *RemoteReply) Decode() (*Reply, error) {
	return DecodeReply(r.Raw)
}

// Return value from the `disc` command
type Discovered struct {
	Peer_sn string `xml:"SN,attr"`
}

// This tag is present if the command must be confirmed (repeated)
type ConfirmationReq struct {
	XMLName xml.Name `xml:"ConfirmationRequired"`
}

// Return value from the `gethd` command.
type HardwareData struct {
	XMLName xml.Name `xml:"HardwareData"`
	Type    string   `xml:"DeviceType,attr"`
	Sn      string   `xml:"SerialNumber,attr"`
	Fw      string   `xml:"FirmwareVersion"`
}

// Event log entries
type Event struct {
	Type  string `xml:"type,attr"`
	Count int    `xml:"Count,attr"`
}

type EventSummary struct {
	XMLName xml.Name `xml:"EventSummary"`
	Count   int      `xml:"numEvents,attr"`
}

type CouplerTest struct {
	XMLName xml.Name `xml:"CableCouplerTest"`
	Status  string   `xml:"status,attr"`
	Z       int      `xml:"Z,attr"`
}

// Structure to hold the parsed XML response from the device.
type Reply struct {
	XMLName     xml.Name `xml:"Reply"`
	Error       errImm
	ErrorAlt    errImmAlt
	Timeout     ErrTimeout
	Warn        string `xml:"WARNING"`
	Confirm     ConfirmationReq
	RemoteReply RemoteReply
	Disc        []Discovered `xml:"Discovery>Discovered"`
	Hd          HardwareData
	Summary     EventSummary
	Events      []Event `xml:"EventList>Event"`
	Tcc         CouplerTest
	rawText     string
}

func (r *Reply) GetRawText() string {
	return r.rawText
}

func (r *Reply) Peers() []string {
	var peers []string
	for _, p := range r.Disc {
		peers = append(peers, p.Peer_sn)
	}

	return peers
}

var ErrConfirm = errors.New("Command must be confirmed")

func removeCtl(r rune) rune {
	if r < '\x20' && !unicode.IsSpace(r) {
		return -1
	}

	return r
}

func DecodeReply(resp []byte) (*Reply, error) {
	// Filter out control characters
	resp = bytes.Map(removeCtl, resp)

	// We need to wrap the raw reply in a <Reply> tag because it is not
	// a single XML element.
	buf := []byte("<Reply>")
	v := Reply{}
	buf = append(append(buf, resp...), []byte("</Reply>")...)

	err := xml.Unmarshal(buf, &v)
	if err != nil {
		return &v, err
	}

	switch {
	case v.Error.Raised():
		return &v, v.Error.ErrImm
	case v.ErrorAlt.Raised():
		return &v, v.ErrorAlt.ErrImm
	case v.Timeout.Raised():
		return &v, &v.Timeout
	case v.Confirm.XMLName.Local == "ConfirmationRequired":
		return &v, ErrConfirm
	}

	return &v, nil
}
