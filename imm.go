// Package IMM provides a communication interface to a Seabird Inductive Modem Module.
package imm

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"sync"
)

const (
	// Command terminator
	EOL = "\r\n"
	// Command prompt
	PROMPT = "IMM>"
)

type Mode int

const (
	// Accepts output from the remote host
	ModeIMService Mode = iota
	// Accepts commands from the local host
	ModeHost Mode = iota
)

var ErrCommand = errors.New("Invalid command")
var ErrMode = errors.New("Wrong mode")

func readResponse(rdr io.Reader, match []byte) (string, error) {
	c := make([]byte, 1)
	buf := make([]byte, 0)
	n, err := rdr.Read(c)
	for n > 0 {
		buf = append(buf, c[0])
		if bytes.HasSuffix(buf, match) {
			break
		}
		n, err = rdr.Read(c)
		if err != nil {
			break
		}
	}
	return string(buf), err
}

func errNotTimeout(err error) error {
	switch {
	case errors.Is(err, io.EOF):
		return nil
	case errors.Is(err, os.ErrDeadlineExceeded):
		return nil
	}

	return err
}

func readResponseCtx(ctx context.Context, rdr io.Reader, match []byte) (string, error) {
	c := make([]byte, 1)
	buf := make([]byte, 0)
	n, err := rdr.Read(c)
	for {
		select {
		case <-ctx.Done():
			return string(buf), ctx.Err()
		default:
		}

		if n > 0 {
			buf = append(buf, c[0])
			if bytes.HasSuffix(buf, match) {
				return string(buf), nil
			}
		}

		n, err = rdr.Read(c)
		if err = errNotTimeout(err); err != nil {
			return string(buf), err
		}
	}
}

// Device controls an IMM attached to a serial port
type Device struct {
	port io.ReadWriter
	mu   *sync.Mutex
	mode Mode
}

// Return a new Device attached to an io.ReadWriter, mode is the
// initial state of the IMM (usually ModeIMService).
func NewDevice(rw io.ReadWriter, mode Mode) *Device {
	return &Device{
		port: rw,
		mu:   &sync.Mutex{},
		mode: mode,
	}
}

func (d *Device) sendCmd(cmd string) error {
	d.port.Write([]byte(cmd))
	_, err := d.port.Write([]byte(EOL))
	return err
}

// Return the current Device mode
func (d *Device) Mode() Mode {
	return d.mode
}

// Wakeup puts the device in Host Service Mode (ModeHost)
func (d *Device) Wakeup() error {
	if d.mode == ModeHost {
		return nil
	}

	d.mu.Lock()
	defer d.mu.Unlock()

	d.port.Write([]byte("\r"))
	_, err := readResponse(d.port, []byte(PROMPT))
	if err == nil {
		d.mode = ModeHost
	}

	return err
}

// Sleep puts the device in IMM Service Mode (ModeIMService)
func (d *Device) Sleep() error {
	if d.mode == ModeIMService {
		return nil
	}

	d.mu.Lock()
	defer d.mu.Unlock()

	d.sendCmd("pwroff")
	_, err := readResponse(d.port, []byte("<PowerOff/>"))
	if err == nil {
		d.mode = ModeIMService
	}

	return err
}

// Listen puts the device in IMM Service Mode and returns an io.ReadWriter which
// can be used to reply to messages from a remote node.
func (d *Device) Listen() (io.ReadWriter, error) {
	if d.mode == ModeIMService {
		return d.port, nil
	}

	err := d.Sleep()
	return d.port, err
}

// Exec sends a command to the device and returns the reply
func (d *Device) Exec(cmd string) (*Reply, error) {
	if d.mode != ModeHost {
		return nil, ErrMode
	}

	d.mu.Lock()
	defer d.mu.Unlock()

	d.sendCmd(cmd)
	resp, err := readResponse(d.port, []byte(PROMPT))
	if err != nil {
		return nil, err
	}

	reply, err := DecodeReply([]byte(resp))
	reply.rawText = resp

	return reply, err
}

// RemoteExec sends a command to a peer modem and returns the reply
func (d *Device) RemoteExec(cmd string, peer Node) (*Reply, error) {
	reply, err := d.Exec(fmt.Sprintf("!%s%s", peer, cmd))
	if err != nil {
		return nil, err
	}

	return reply.RemoteReply.Decode()
}

// HostExec sends a command to the host of a peer modem and returns the
// response text.
func (d *Device) HostExec(cmd string, peer Node) (string, error) {
	reply, err := d.Exec(fmt.Sprintf("#%s%s", peer, cmd))
	if err != nil {
		return "", err
	}

	return reply.RemoteReply.Text, nil
}

// Set updates a device configuration setting
func (d *Device) Set(key string, value interface{}) error {
	_, err := d.Exec(fmt.Sprintf("set%s=%v", key, value))
	return err
}

// Interrupt stops the IMM from listening for a reply from a Client
func (d *Device) Interrupt() error {
	if d.mode != ModeHost {
		return ErrMode
	}

	d.mu.Lock()
	defer d.mu.Unlock()

	d.port.Write([]byte("\x1b"))
	_, err := readResponse(d.port, []byte(PROMPT))

	return err
}
