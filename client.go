package imm

import (
	"context"
	"fmt"
	"io"
	"sync"
	"time"
)

// Node represents an endpoint on the inductive network
type Node interface {
	Addr() string // node address
}

// IMM ID 0-99
type DevID uint

func (d DevID) Addr() string {
	return fmt.Sprintf("%02d", uint(d))
}

// IMM serial number
type DevSn string

func (d DevSn) Addr() string {
	return fmt.Sprintf("S%s:", d)
}

// Response implements an io.ReadCloser on the reply from
// a Node
type Response struct {
	rdr    io.Reader
	closed bool
}

func (r *Response) Read(p []byte) (int, error) {
	if r.closed {
		return 0, io.ErrClosedPipe
	}
	return r.rdr.Read(p)
}

func (r *Response) ReadUntil(ctx context.Context, delim string) (string, error) {
	if r.closed {
		return "", io.ErrClosedPipe
	}
	return readResponseCtx(ctx, r.rdr, []byte(delim))
}

func (r *Response) Close() error {
	if r.closed {
		return nil
	}
	_, err := readResponse(r.rdr, []byte(PROMPT))
	r.closed = true
	return err
}

// Client implements the "buoy controller" side of an IMM
// message exchange.
type Client struct {
	rw   io.ReadWriter
	peer Node
	mu   *sync.Mutex
}

// Connect returns a Client which can be used to send messages to the host
// connected to a remote node peer. This is only needed if the host sends
// binary data, otherwise [HostExec] is easier to use.
func (d *Device) Connect(peer Node) (*Client, error) {
	_, err := d.Exec("fcl")
	if err != nil {
		return nil, err
	}

	return &Client{rw: d.port, peer: peer, mu: d.mu}, nil
}

func retry(attempts int, sleep time.Duration, f func() error) (err error) {
	for i := 0; i < attempts; i++ {
		if i > 0 {
			time.Sleep(sleep)
			sleep *= 2
		}
		err = f()
		if err == nil {
			return nil
		}
	}
	return fmt.Errorf("after %d attempts, last error: %s", attempts, err)
}

// ConnectWait works like [Connect] but does not force-capture the line. It
// makes attempts tries to capture the line using a exponential
// back-off. Delay is the wait-time between the first and second tries.
func (d *Device) ConnectWait(peer Node, attempts int, delay time.Duration) (*Client, error) {
	err := retry(attempts, delay, func() error {
		_, err := d.Exec("captureline")
		return err
	})
	if err != nil {
		return nil, err
	}

	return &Client{rw: d.port, peer: peer, mu: d.mu}, nil
}

// Request sends a command to a Node and returns a Response which can
// be read to obtain the reply from the Node host. The Response must be
// closed after reading.
func (c *Client) Request(ctx context.Context, cmd string) (*Response, error) {
	c.mu.Lock()
	defer c.mu.Unlock()

	fmt.Fprintf(c.rw, "#%s%s%s", c.peer.Addr(), cmd, EOL)
	_, err := readResponseCtx(ctx, c.rw, []byte("<RemoteReply>"))
	if err != nil {
		return nil, fmt.Errorf("No RemoteReply: %w", err)
	}

	return &Response{rdr: c.rw}, nil
}
