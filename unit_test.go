package imm

import (
	"bytes"
	"context"
	"encoding/xml"
	"errors"
	"reflect"
	"strings"
	"testing"
)

func TestReplyDecode(t *testing.T) {
	table := []struct {
		in, out string
		err     error
	}{
		{
			in:  "<RemoteReply>Hello</RemoteReply>\r\n<Executed/>",
			out: "Hello",
		},
		{
			in:  "<RemoteReply>Hello</RemoteReply>\r\n\x00<Executed/>\x04",
			out: "Hello",
		},
		{
			in:  "<WARNING>Must confirm</WARNING>\r\n<ConfirmationRequired/>\r\n<Executed/>",
			out: "",
			err: ErrConfirm,
		},
		{
			in:  "<ERROR type='FAILED' msg='LINE BUSY'/>\r\n<Executed/>",
			out: "",
			err: ErrImm{Type: "FAILED"},
		},
	}

	for _, e := range table {
		r, err := DecodeReply([]byte(e.in))
		if !errors.Is(err, e.err) {
			t.Errorf("Unexpected error; %#v", err)
			continue
		}
		if out := r.RemoteReply.Text; out != e.out {
			t.Errorf("Bad reply; expected %q, got %q", e.out, out)
		}
	}
}

func TestGethd(t *testing.T) {
	response := `<HardwareData
DeviceType='SBE90554 IMM' SerialNumber='70000047'> <Manufacturer>Sea-Bird Electronics, Inc</Manufacturer> <HardwareVersion>41420B</HardwareVersion>
<HardwareVersion>PCB Type 3, 10345B</HardwareVersion>
<MfgDate>May 4 2013</MfgDate>
<FirmwareVersion>1.14 Jan 13 2012 16:32:44</FirmwareVersion> <FirmwareLoader>MSP LOADER RS232 57.6K 2007-02-08</FirmwareLoader> </HardwareData>
<Executed/>`
	r, err := DecodeReply([]byte(response))
	if err != nil {
		t.Fatal(err)
	}

	expected := "1.14 Jan 13 2012 16:32:44"
	if r.Hd.Fw != expected {
		t.Errorf("Decode error; expected %q, got %q", expected, r.Hd.Fw)
	}
}

func TestRemoteXML(t *testing.T) {
	response := `<RemoteReply>
<EventSummary numEvents='19'/>
<EventList DeviceType='SBE90554 IMM' SerialNumber='70000047'> <Event type='PowerOnReset' Count='18'/>
<Event type='THost0' Count='1'/>
</EventList>
<Executed/>
</RemoteReply>
<Executed/>
`
	r, err := DecodeReply([]byte(response))
	if err != nil {
		t.Fatal(err)
	}

	v, err := r.RemoteReply.Decode()
	if err != nil {
		t.Fatal(err)
	}

	if n := len(v.Events); n != 2 {
		t.Errorf("Bad list length; expected 2, got %d", n)
	}

	count := int(0)
	for _, e := range v.Events {
		count += e.Count
	}

	if count != v.Summary.Count {
		t.Errorf("Bad event count; expected %d, got %d", v.Summary.Count, count)
	}
}

func TestDiscovery(t *testing.T) {
	response := `<TimeoutICD>8000</TimeoutICD>
<Discovery>
<Discovered SN='70002445'/>
</Discovery>
<Executed/>`
	r, err := DecodeReply([]byte(response))
	if err != nil {
		t.Fatal(err)
	}

	peers := r.Peers()
	if n := len(peers); n != 1 {
		t.Fatalf("Bad list length; expected 1, got %d", n)
	}

	expected := "70002445"
	if peers[0] != expected {
		t.Errorf("Bad value; expected %q, got %q", expected, peers[0])
	}

}

func TestCC(t *testing.T) {
	response := `<CableCouplerTest status='GOOD' Z='852'/>
<Executed/>`
	r, err := DecodeReply([]byte(response))
	if err != nil {
		t.Fatal(err)
	}

	expected := CouplerTest{
		XMLName: xml.Name{Local: "CableCouplerTest"},
		Status:  "GOOD",
		Z:       852,
	}

	if !reflect.DeepEqual(r.Tcc, expected) {
		t.Errorf("Decode failed; expected %#v, got %#v", expected, r.Tcc)
	}
}

func TestClient(t *testing.T) {
	in := "IMM><RemoteReply>Hello world\r\n</RemoteReply>\r\n<Executed/>\r\nIMM>"
	rw := bytes.NewBuffer([]byte(in))

	dev := NewDevice(rw, ModeHost)
	cln, err := dev.Connect(DevSn("12345678"))

	resp, err := cln.Request(context.Background(), "ohi")
	if err != nil {
		t.Fatal(err)
	}

	reply, err := resp.ReadUntil(context.Background(), "\r\n")
	if err != nil {
		t.Fatalf("Malformed response: %v", err)
	}

	if s := strings.TrimRight(reply, " \r\n"); s != "Hello world" {
		t.Errorf("Unexpected reply: %q", s)
	}

	if err := resp.Close(); err != nil {
		t.Errorf("Cannot close response: %v", err)
	}

	expected := "fcl\r\n#S12345678:ohi\r\n"
	if sent := rw.String(); sent != expected {
		t.Errorf("Bad outgoing message; expected %q, got %q", expected, sent)
	}

}

func TestSet(t *testing.T) {
	table := []struct {
		key string
		val interface{}
		out string
	}{
		{
			key: "thost3",
			val: 1000,
			out: "setthost3=1000\r\n",
		},
	}

	rw := bytes.NewBuffer([]byte(PROMPT))
	dev := NewDevice(rw, ModeHost)
	for _, e := range table {
		err := dev.Set(e.key, e.val)
		if err != nil {
			t.Fatal(err)
		}
		if sent := rw.String(); sent != e.out {
			t.Errorf("Bad outgoing message; expected %q, got %q", e.out, sent)
		}
	}

}
